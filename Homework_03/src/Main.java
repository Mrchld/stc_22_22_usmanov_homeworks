import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[] array = new int[N];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        int count = 0;
        for (int i = 0; i < array.length; i++) {
            boolean leftBigger = (i == 0) || (array[i - 1] > array[i]);
            boolean rightBigger = (i == array.length - 1) || (array[i + 1] > array[i]);
            if (leftBigger && rightBigger) {
                count += 1;
            }
        }
        System.out.println(count);
    }

}