import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

final class Main {
    public static void main (String[] args) {

        ProductRepository productRepositoryImpl = new ProductRepositoryImpl();
        // по ID
        Integer id = 2;
        Product produсtById = productRepositoryImpl.findById(id); 
        System.out.println("по id = " + id.toString());           
        System.out.println(produсtById.toString());


        // по подстроке
        String title = "ol";
        List<Product> prodcutsByTitle = productRepositoryImpl.findAllByTitleLike(title);
        System.out.println("по подстроке = " + title);
        for (Product productByTitle : prodcutsByTitle) {
            System.out.println(productByTitle.toString());
        }

        // обновление по id = 1
        Product productForUpdate = productRepositoryImpl.findById(1); 
        productForUpdate.setCount(9999);
        productForUpdate.setPrice(9999.9);
        productRepositoryImpl.update(productForUpdate);
    }
}

final class Product {

    Integer id;
    String name;
    Double price;
    Integer left;

    public Product(Integer id,
                   String name,
                   Double price,
                   Integer left) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.left = left;
    }

    public void setCount(int count) {
        this.left = count;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String toString (){
        return this.id.toString() + "|" + this.name.toString() + "|" + this.price.toString() + "|" + this.left.toString(); 
    }
}

interface ProductRepository {
    List<Product> findAllByTitleLike(String title);

    Product findById(Integer id);

    void update(Product product);
}

final class ProductRepositoryImpl implements ProductRepository {

    private List<Product> products = new ArrayList<>();

    ProductRepositoryImpl() {
        try {
            File file = new File("src\\products.txt");
            Scanner br = new Scanner(file);
            String line = br.nextLine();

            while (!Objects.equals(line, "")) {
                String[] splittedLine = line.split("\\|");
                int Id = Integer.parseInt(splittedLine[0]);
                String name = splittedLine[1];
                Double price = Double.parseDouble(splittedLine[2]);
                int left = Integer.parseInt(splittedLine[3]);
                Product newProduct = new Product(Id, name, price, left);
                products.add(newProduct);
                if (br.hasNextLine()) {
                    line = br.nextLine();
                } else {
                    break;
                }
            }
            br.close();
        } catch (FileNotFoundException fnfe) {
            System.out.println("File not found");
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        List<Product> filteredBySubString = new ArrayList<>();
        for (Product product : products) {
            if (product.name.contains(title)) {
                filteredBySubString.add(product);
            }
        }
        return filteredBySubString;
    }

    @Override
    public Product findById(Integer id) {
        Product filteredById = null;
        for (Product product : this.products) {
            if (Objects.equals(product.id, id)) {
                filteredById = product;
            }
        }
        return filteredById;
    }

    @Override
    public void update(Product product) {
        try {
            PrintWriter writer = new PrintWriter("products-updated.txt", StandardCharsets.UTF_8);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);

            for (Product p : this.products) {
                bufferedWriter.write(p.toString());
                bufferedWriter.newLine();
            }

            bufferedWriter.close();
            writer.close();
        } catch (FileNotFoundException fnfe) {
            System.out.println("File not found");
        } catch (UnsupportedEncodingException uee) {
            System.out.println("Unsupported encoding");
        } catch (IOException ioe) {
            System.out.println("IOExeption");
        }
    }
}