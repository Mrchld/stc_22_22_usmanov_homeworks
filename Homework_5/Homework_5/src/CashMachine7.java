public class CashMachine7 {

    public final int MAX_GIVE_OUT;
    public final int MAX_MONEY;

    private int money = 0;
    private int operationsAmount = 0;

    public CashMachine7(int maxGiveOut, int maxMoney) {
        this.MAX_GIVE_OUT = maxGiveOut;
        this.MAX_MONEY = maxMoney;
    }

    public int giveOut(int requestedAmount) {
        int givenAmount = requestedAmount;
        givenAmount = Math.min(givenAmount, MAX_GIVE_OUT);
        givenAmount = Math.min(givenAmount, money);
        operationsAmount += 1;
        money -= givenAmount;
        return givenAmount;
    }

    public int putIn(int requestedAmount) {
        operationsAmount += 1;
        int putMoney = Math.min(requestedAmount, MAX_MONEY - money);
        money += putMoney;
        return requestedAmount - putMoney;
    }

    public int getOperationsAmount() {
        return operationsAmount;
    }
    public static void main(String []args){
        CashMachine7 cm = new CashMachine7(60, 100);
        System.out.println(cm.putIn(5)); // 0
        System.out.println(cm.putIn(1000)); // 905
        System.out.println(cm.giveOut(30)); // 30
        System.out.println(cm.giveOut(70)); // 60
        System.out.println(cm.giveOut(1000)); // 10
        System.out.println(cm.getOperationsAmount()); // 5
    }

}
