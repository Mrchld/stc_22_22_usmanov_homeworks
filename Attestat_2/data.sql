insert into schema.driver (first_name, last_name, phone_numb, exp_drive, age, drive_pass, category_pass, rating)
values ('Газбек', 'Ахматович', '7800001221', '10', 55, 'true', 'B','5');
insert into schema.driver (first_name, last_name, phone_numb, exp_drive, age, drive_pass, category_pass, rating)
values ('Марат', 'Усманов', '7800301221', '5', '25', 'true', 'B1','4');
insert into schema.driver (first_name, last_name, phone_numb, exp_drive, age, drive_pass, category_pass, rating)
values ('Данил', 'Иванов', '7800002521', '0', '18', 'false', 'not','0');
insert into schema.driver (first_name, last_name, phone_numb, exp_drive, age, drive_pass, category_pass, rating)
values ('Дамир', 'Сыртланов', '7800114221', '1', '28', 'true', 'B','0');
insert into schema.driver (first_name, last_name, phone_numb, exp_drive, age, drive_pass, category_pass, rating)
values ('Елена', 'Редисовна', '7921001221', '4', '33', 'true', 'A','3');

insert into schema.auto (model_auto, color_auto, numb_auto, id_driver)
values ('UAZ 14','blue','a123kc','1');
insert into schema.auto (model_auto, color_auto, numb_auto, id_driver)
values ('audi','black','a151tc','2');
insert into schema.auto (model_auto, color_auto, numb_auto, id_driver)
values ('lexus','grey','c432ma','2');
insert into schema.auto (model_auto, color_auto, numb_auto, id_driver)
values ('mazda','white','a673kc','1');
insert into schema.auto (model_auto, color_auto, numb_auto, id_driver)
values ('mini','pink','o345pc','5');

insert into schema.trip (id_driver, id_auto, date_trip, duration_trip)
values ('1', '1','2022-10-10','120');
insert into schema.trip (id_driver, id_auto, date_trip, duration_trip)
values ('2', '1','2022-5-7','20');
insert into schema.trip (id_driver, id_auto, date_trip, duration_trip)
values ('3', '2','2022-9-10','25');
insert into schema.trip (id_driver, id_auto, date_trip, duration_trip)
values ('4', '5','2022-1-9','50');
insert into schema.trip (id_driver, id_auto, date_trip, duration_trip)
values ('5', '2','2022-4-10','77');