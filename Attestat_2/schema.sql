create schema schema;

create table schema.driver (
                               id bigserial primary key,
                               first_name char(20),
                               last_name char(20),
                               phone_numb char(20),
                               exp_drive integer check (exp_drive >= 0),
                               age integer check (age >= 18 and age <= 120),
                               drive_pass bool,
                               category_pass char (5),
                               rating integer check (rating >= 0 and rating <=5)
);

create table schema.auto (
                             id bigserial primary key,
                             model_auto char(20),
                             color_auto char (15),
                             numb_auto char (10),
                             id_driver integer not null

);
create table schema.trip (
                             id bigserial primary key,
                             id_driver integer,
                             id_auto integer,
                             date_trip date,
                             duration_trip integer

);
