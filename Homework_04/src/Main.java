public class Main {

    public static void main(String []args){
        printEven(new int[]{89, 1, 4, 92, 3, 88, 2, 33, 47, 26, 12, 48, 89});
    }

    public static void printEven(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                System.out.println(arr[i]);
            }
        }
    }

}