public class Main2 {

    public static void main(String[] args){
        int[] number = {9, 8, 7, 2, 3};
        int result = toInt(number);
        System.out.println(result);
    }

    public static int toInt(int[] array) {
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            int digit = array[i];
            result = result*10 + digit;
        }
        return result;
    }

}
